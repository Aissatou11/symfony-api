<?php

namespace App\Controller;

use App\Entity\Region;
use App\Repository\RegionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    #[Route('/api/regions', name: 'get_regions_api', methods:"GET")]
    public function addRegionByApi(SerializerInterface $serializer, EntityManagerInterface $em): Response
    {
            // Recuperation des Regions en Json
            $regionJson = file_get_contents("https://geo.api.gouv.fr/regions");

/*
    // Method 1
            // Decode Json to Array
            $regionTab = $serializer->decode($regionJson,"json");

            // Denormalize Array to Object
            $regionObject = $serializer->denormalize($regionTab, "App\Entity\Region[]");
*/
        

    // Method 2 
            // Deserialize JSON to Object
            $regionObject = $serializer->deserialize($regionJson,'App\Entity\Region[]','json');
            
             foreach ($regionObject as $region) {
                 $em->persist($region);
             }
            $em->flush();
            

        return new JsonResponse("success", Response::HTTP_CREATED, [], true);
    }

    #[Route('/api/show_regions', name: 'get_regions_api_BD', methods:"GET")]
    public function showRegion(SerializerInterface $serializer,RegionRepository $regionRepository)
    {
        // Recupérer tous les régions dans la base de données
        $regionsObject=$regionRepository->findAll();

        // Serialize Object to JSON
        $regionsJson =$serializer->serialize(
            $regionsObject,
            "json",
            [
            "groups"=>["region:read"]
            ]
        );

        return new JsonResponse($regionsJson,Response::HTTP_OK,[],true);
    }

    #[Route('/api/post_regions', name: 'post_regions_api_BD', methods:"POST")]
    public function addRegion(Request $request, SerializerInterface $serializer,    EntityManagerInterface $em, ValidatorInterface $validator )
    {
        // Recupérer le contenu du body de la requête
        $regionJson = $request->getContent();

        // Deserialize JSON to Object
        $region = $serializer->deserialize($regionJson, Region::class,'json');

        // Validation des données 
        $errors = $validator->validate($region);
        if (count($errors) > 0) {
        $errorsString =$serializer->serialize($errors,"json");
        return new JsonResponse($errorsString ,Response::HTTP_BAD_REQUEST,[],true);
        }

        $em->persist($region);
        $em->flush();
        return new JsonResponse("succes",Response::HTTP_CREATED,[],true);
    }

}
