<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Profil;
use App\Entity\Commune;
use App\Entity\Departement;
use App\Repository\RegionRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $repo;
    function __construct(RegionRepository $repo, UserPasswordHasherInterface $passwordHasher)
    {
        $this->repo = $repo;
        $this->passwordHasher = $passwordHasher;

    }
    public function load(ObjectManager $manager): void
    {
        $regions = $this->repo->findAll();
        $faker = Factory::create('fr_FR');
        foreach ($regions as $region) {
            $departement = new Departement();
            $departement->setCode($faker->postcode())
                        ->setNom($faker->city())
                        ->setRegion($region);
            $manager->persist($departement);

            //Pour chaque Département, on insére 10 Communes
            for ($i=0; $i <10 ; $i++) {
                $commune=new Commune();
                $commune->setCode($faker->postcode())
                ->setCommune($faker->city())
                ->setDepartement($departement);
                $manager->persist($commune);
                }
        }

        $manager->flush();

        $profils =["ADMIN" ,"FORMATEUR" ,"APPRENANT" ,"CM"];
        foreach ($profils as $key => $libelle) 
        {
            $profil =new Profil() ;
            $profil ->setLibelle ($libelle );
            $manager ->persist ($profil );
            $manager ->flush();
            for ($i=1; $i <=3 ; $i++) 
            {
                $user = new User() ;
                $user ->setProfil ($profil )
                      ->setLogin (strtolower ($libelle ).$i)
                      ->setNomComplet($faker->name());
                //Génération des Users
                $hasher = $this->passwordHasher->hashPassword($user, 'passer123' );
                $user ->setPassword ($hasher );
                $manager ->persist ($user);
            }
            $manager ->flush();
        }
    }

        

        
}
